using UnityEngine;

public class Pawn : MonoBehaviour
{
    public int playerID; // Identifiant du joueur
    public Vector3 currentPosition; // Position actuelle du pion sur le plateau
    public int pawnValue; // Valeur du pion

    private void Start()
    {
        currentPosition = transform.position;
    }

    public void MoveToPosition(Vector3 newPosition)
    {
        // Mettre à jour la position actuelle du pion
        currentPosition = newPosition;
        // Déplacer le pion vers la nouvelle position
        transform.position = new Vector3(newPosition.x, 1f, newPosition.z);
    }

    
}