using TMPro;
using UnityEngine;

public class BoardGenerator : MonoBehaviour
{
    public GameObject pawnPrefab;
    public GameObject cubePrefab;
    public GameObject cubePrefab2;
    public int numPawnsPerPlayer = 33;
    public TextMeshPro selectedPawnValueText0;
    public TextMeshPro selectedPawnValueText1;
    public bool isUpdateEnabled = true;
    public TextMeshPro res0;
    public TextMeshPro res1;


    private Pawn selectedPawn;
    private int currentPlayer = 0;
    private int drapeau0 = 3;
    private int drapeau1 = 1;
    private int jeton0 = 33;
    private int jeton1 = 33;
    private bool etape01 = false;
    private bool etape02 = false;
    private bool etape03 = false;
    private bool etape11 = false;
    private bool etape12 = false;
    private bool etape13 = false;
    

    void Start()
    {
        GeneratePawns();
    }

    void GeneratePawns()
    {
        GeneratePawnsForPlayer(0, 0, 3, Color.green); // Génération des pions de l'équipe 0 sur les 4 premières lignes
        GeneratePawnsForPlayer(1, 6, 9, Color.blue); // Génération des pions de l'équipe 1 sur les 2 dernières lignes
    }

    void GeneratePawnsForPlayer(int playerID, int minRow, int maxRow, Color pawnColor)
    {
        for (int i = 0; i < 1; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 10;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 1; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 9;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 2; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 8;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 3; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 7;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 4; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 6;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 4; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 5;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 4; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 4;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 5; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 3;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 8; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 2;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }
        
        for (int i = 0; i < 1; i++)
        {
            bool isPositionValid = false;
            Vector3 position = Vector3.zero;

            // Tant que la position n'est pas valide, continuez à en générer une nouvelle
            while (!isPositionValid)
            {
                int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;

                // Vérifiez si la position est déjà occupée par un autre pion
                Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                isPositionValid = colliders.Length == 0;
            }

            GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
            Pawn pawnScript = pawnObject.GetComponent<Pawn>();

            pawnScript.playerID = playerID;
            pawnScript.currentPosition = position;
            pawnScript.pawnValue = 1;

            // Définir la couleur du pion en fonction de l'équipe
            pawnScript.GetComponent<Renderer>().material.color = pawnColor;
        }

        if (playerID == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                bool isPositionValid = false;
                Vector3 position = Vector3.zero;

                // Tant que la position n'est pas valide, continuez à en générer une nouvelle
                while (!isPositionValid)
                {
                    int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                    Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                    position = offset;

                    // Vérifiez si la position est déjà occupée par un autre pion
                    Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                    isPositionValid = colliders.Length == 0;
                }

                GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
                Pawn pawnScript = pawnObject.GetComponent<Pawn>();

                pawnScript.playerID = playerID;
                pawnScript.currentPosition = position;
                pawnScript.pawnValue = 0;

                // Définir la couleur du pion en fonction de l'équipe
                pawnScript.GetComponent<Renderer>().material.color = pawnColor;
            }
            
        }
        else
        {
            for (int i = 0; i < 1; i++)
            {
                bool isPositionValid = false;
                Vector3 position = Vector3.zero;

                // Tant que la position n'est pas valide, continuez à en générer une nouvelle
                while (!isPositionValid)
                {
                    int randomRow = Random.Range(minRow, maxRow + 1); // Choix aléatoire de la ligne
                    Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 1, randomRow); // Offset pour répartir les pions sur différentes positions centrées
                    position = offset;

                    // Vérifiez si la position est déjà occupée par un autre pion
                    Collider[] colliders = Physics.OverlapSphere(position, 0.1f);
                    isPositionValid = colliders.Length == 0;
                }

                GameObject pawnObject = Instantiate(pawnPrefab, position, Quaternion.identity);
                Pawn pawnScript = pawnObject.GetComponent<Pawn>();

                pawnScript.playerID = playerID;
                pawnScript.currentPosition = position;
                pawnScript.pawnValue = 0;

                // Définir la couleur du pion en fonction de l'équipe
                pawnScript.GetComponent<Renderer>().material.color = pawnColor;
            }
        }
    }

    void Update()
    {
        if (!isUpdateEnabled)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                GameObject hitObject = hit.collider.gameObject;
                Pawn pawn = hitObject.GetComponent<Pawn>();

                if (pawn != null && pawn.playerID==currentPlayer)
                {
                    // Sélection du pion
                    if (selectedPawn != null)
                    {
                        // Désélection du pion précédemment sélectionné
                        selectedPawn.GetComponent<Renderer>().material.color = GetTeamColor(selectedPawn.playerID);
                        selectedPawnValueText0.text = "Valeur: -";
                        selectedPawnValueText1.text = "Valeur: -";

                    }

                    selectedPawn = pawn;
                    selectedPawn.GetComponent<Renderer>().material.color = Color.red;
                    if (selectedPawn.playerID == 0)
                    {
                        selectedPawnValueText0.text = "Valeur: " + selectedPawn.pawnValue.ToString();
                    }
                    else
                    {
                        selectedPawnValueText1.text = "Valeur: " + selectedPawn.pawnValue.ToString();
                    }
                    
                }
                else if (selectedPawn != null)
                {
                    // Déplacement du pion
                    Vector3 targetPosition = hitObject.transform.position;
                    Vector3 newPosition = new Vector3(targetPosition.x, 1f, targetPosition.z); // Réglez la hauteur y à 1

                    if (IsMoveValid(selectedPawn.currentPosition, newPosition))
                    {
                        Collider[] colliders = Physics.OverlapSphere(newPosition, 0.1f);
                        bool isDestinationOccupied = false;
                        Pawn otherPawn = null;
                        bool competitor = false;

                        foreach (Collider collider in colliders)
                        {
                            Pawn pawnInDestination = collider.GetComponent<Pawn>();
                            if (pawnInDestination != null)
                            {
                                
                                if (pawnInDestination.playerID == selectedPawn.playerID)
                                {
                                    isDestinationOccupied = true;
                                }
                                else
                                {
                                    otherPawn = pawnInDestination;
                                    competitor = true;
                                }
                                break;
                            }
                        }
                        
                        if (!isDestinationOccupied && !competitor)
                        {
                            selectedPawn.MoveToPosition(newPosition);
                            selectedPawn.currentPosition = newPosition;
                            selectedPawn.GetComponent<Renderer>().material.color = GetTeamColor(selectedPawn.playerID);
                            selectedPawnValueText0.text = "Valeur: -";
                            selectedPawnValueText1.text = "Valeur: -";
                            selectedPawn = null;
                            currentPlayer = (currentPlayer + 1) % 2;
                        }
                        if (otherPawn != null)
                        {
                            int comparison = selectedPawn.pawnValue.CompareTo(otherPawn.pawnValue);
                            if (comparison > 0) // Le pion sélectionné a une valeur plus grande
                            {
                                selectedPawn.MoveToPosition(newPosition);
                                selectedPawn.currentPosition = newPosition;
                                if (otherPawn.playerID == 0 && otherPawn.pawnValue != 0)
                                {
                                    jeton0 = jeton0 - 1;
                                }
                                else if (otherPawn.playerID == 0 && otherPawn.pawnValue == 0)
                                {
                                    drapeau0 = drapeau0 - 1;
                                }
                                else if (otherPawn.playerID == 1 && otherPawn.pawnValue != 0)
                                {
                                    jeton1 = jeton1 - 1;
                                }
                                else if (otherPawn.playerID == 1 && otherPawn.pawnValue == 0)
                                {
                                    drapeau1 = drapeau1 - 1;
                                }
                                Destroy(otherPawn.gameObject); // Supprimer le pion adverse
                                selectedPawn.GetComponent<Renderer>().material.color = GetTeamColor(selectedPawn.playerID);
                                selectedPawnValueText0.text = "Valeur: -";
                                selectedPawnValueText1.text = "Valeur: -";
                                selectedPawn = null;
                                currentPlayer = (currentPlayer + 1) % 2;
                            }
                            else if (comparison == 0) // Les deux pions ont la même valeur
                            {
                                if (otherPawn.pawnValue != 0)
                                {
                                    jeton0 = jeton0 - 1;
                                    jeton1 = jeton1 - 1;
                                }
                                else
                                {
                                    drapeau0 = drapeau0 - 1;
                                    drapeau1 = drapeau1 - 1;
                                }
                                Destroy(selectedPawn.gameObject); // Supprimer le pion sélectionné
                                Destroy(otherPawn.gameObject); // Supprimer le pion adverse
                                selectedPawnValueText0.text = "Valeur: -";
                                selectedPawnValueText1.text = "Valeur: -";
                                currentPlayer = (currentPlayer + 1) % 2;
                            }
                            else if (comparison < 0) // Si le pion sélectionné a une valeur inférieure
                            {
                                if (selectedPawn.playerID == 0 && selectedPawn.pawnValue != 0)
                                {
                                    jeton0 = jeton0 - 1;
                                }
                                else if (selectedPawn.playerID == 0 && selectedPawn.pawnValue == 0)
                                {
                                    drapeau0 = drapeau0 - 1;
                                }
                                else if (selectedPawn.playerID == 1 && selectedPawn.pawnValue != 0)
                                {
                                    jeton1 = jeton1 - 1;
                                }
                                else if (selectedPawn.playerID == 1 && selectedPawn.pawnValue == 0)
                                {
                                    drapeau1 = drapeau1 - 1;
                                }
                                Destroy(selectedPawn.gameObject); // Supprimer le pion sélectionné
                                selectedPawnValueText0.text = "Valeur: -";
                                selectedPawnValueText1.text = "Valeur: -";
                                currentPlayer = (currentPlayer + 1) % 2;
                            }
                        }
                    }
                }
            }
        }

        if (jeton0 < 25 && !etape01)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 position = Vector3.zero;
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 0, Random.Range(0, 9 + 1)); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;
                GameObject cubeObject = Instantiate(cubePrefab, position, Quaternion.identity);
            }
            etape01 = true;
        }
        if (jeton0 < 13 && !etape02)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 position = Vector3.zero;
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 0, Random.Range(0, 9 + 1)); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;
                GameObject cubeObject = Instantiate(cubePrefab, position, Quaternion.identity);
            }
            etape02 = true;
        }
        if (jeton0 < 5 && !etape03)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 position = Vector3.zero;
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 0, Random.Range(0, 9 + 1)); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;
                GameObject cubeObject = Instantiate(cubePrefab, position, Quaternion.identity);
            }
            etape03 = true;
        }
        
        if (jeton1 < 25 && !etape11)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 position = Vector3.zero;
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 0, Random.Range(0, 9 + 1)); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;
                GameObject cubeObject = Instantiate(cubePrefab2, position, Quaternion.identity);
            }
            etape11 = true;
        }
        if (jeton1 < 13 && !etape12)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 position = Vector3.zero;
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 0,Random.Range(0, 9 + 1)); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;
                GameObject cubeObject = Instantiate(cubePrefab2, position, Quaternion.identity);
            }
            etape12 = true;
        }
        if (jeton1 < 5 && !etape13)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 position = Vector3.zero;
                Vector3 offset = new Vector3(Random.Range(0, 9 + 1), 0, Random.Range(0, 9 + 1)); // Offset pour répartir les pions sur différentes positions centrées
                position = offset;
                GameObject cubeObject = Instantiate(cubePrefab2, position, Quaternion.identity);
            }
            etape13 = true;
        }

        if (jeton0 == 0 || drapeau0 == 0)
        {
            res0.text = "Perdu";
            res1.text = "Gagné !";
            isUpdateEnabled = false;
        }
        else if (jeton1 == 0 || drapeau1 == 0)
        {
            res0.text = "Gagné !";
            res1.text = "Perdu";
            isUpdateEnabled = false;
        }
    }
    

    bool IsMoveValid(Vector3 currentPosition, Vector3 newPosition)
    {
        bool isAdjacent = false;
        float distance = Vector3.Distance(currentPosition, newPosition);
        if (selectedPawn.pawnValue == 2)
        {
            isAdjacent = distance <= 10f;
            return isAdjacent;
        }
        if (selectedPawn.pawnValue != 0)
        {
            isAdjacent = distance <= 1.2f;
            return isAdjacent;
        }
        return isAdjacent;

    }

    Color GetTeamColor(int playerID)
    {
        if (playerID == 0)
            return Color.green;
        else if (playerID == 1)
            return Color.blue;
        
        return Color.white;
    }

    
}
